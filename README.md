
DsLav is an open source, free media player, for Windows XP SP3 and above.

Visit the website for information and to download installers and sourcecode: https://dslav.codeberg.page

Note this project has been hosted at https://www.dslav.tuxfamily.org for some time, but they seem to be having issues - downloads are no longer available there and may not get fixed.
Since the current release, version 0.9.1.0, was built to use files at TuxFamily, the app's "Check For Updates" button will not work. The next release will fix.

GNU General Public Version 2 licence.
